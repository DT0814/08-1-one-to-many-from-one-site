package com.twuc.webApp.domain.oneToMany.withJoinColumn;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Objects;

/**
 * @author tao.dong
 */ // TODO
//
// ChildEntity 应当具备一个自动生成的 id 以及一个字符串 name。请实现 ChildEntity。ChildEntity 的
// 数据表的参考定义如下：
//
// +───────────────────+──────────────+──────────────────────────────+
// | Column            | Type         | Additional                   |
// +───────────────────+──────────────+──────────────────────────────+
// | id                | bigint       | primary key, auto_increment  |
// | name              | varchar(20)  | not null                     |
// | parent_entity_id  | bigint       | null                         |
// +───────────────────+──────────────+──────────────────────────────+
//
// <--start-
@Entity
public class ChildEntity {
    @Id
    @GeneratedValue
    private Long id;
    @Column(length = 20)
    private String name;


    public ChildEntity() {
    }

    public ChildEntity(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChildEntity)) {
            return false;
        }
        ChildEntity that = (ChildEntity) o;
        return getName().equals(that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
// --end->
